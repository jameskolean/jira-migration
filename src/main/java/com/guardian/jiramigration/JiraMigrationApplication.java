package com.guardian.jiramigration;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

@SpringBootApplication
public class JiraMigrationApplication {
  public static void main(String[] args) {
    ApplicationContext app = SpringApplication.run(JiraMigrationApplication.class, args);
    Task task = app.getBean(Task.class);
    System.out.println(task.getArticles());
  }
  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapper();
  }
  @Bean
  RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
    TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
    SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();
    SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
    CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();
    HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
    requestFactory.setHttpClient(httpClient);
    return new RestTemplate(requestFactory);
  }
  @Bean
  Task task() {
    return new Task();
  }
  class Task {
    @Autowired
    RestTemplate restTemplate;
    private ArticleResult getArticles() {
      String url = "https://newsapi.org/v2/top-headlines?sources=techcrunch&sortBy=top&apiKey=44ddad9741b745168f18d2a558262c68";
      ResponseEntity<ArticleResult> forEntity = restTemplate.getForEntity(url, ArticleResult.class);
      return forEntity.getBody();
    }
  }
}

