package com.guardian.jiramigration;

import com.guardian.jiramigration.jpa.Author;
import com.guardian.jiramigration.jpa.AuthorDto;
import com.guardian.jiramigration.jpa.Book;
import com.guardian.jiramigration.jpa.BookDto;
import com.guardian.jiramigration.jpa.LibraryService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/library")
class LibraryController {

  @Autowired
  LibraryService libraryService;
  @RequestMapping("/author")
  public List<AuthorDto> authors() {
    return libraryService.getAllAuthors();
  }

  @RequestMapping("/book")
  public List<BookDto> books() {
    return libraryService.getAllBooks();
  }
}