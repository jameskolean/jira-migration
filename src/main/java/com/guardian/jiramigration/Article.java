package com.guardian.jiramigration;

import lombok.Data;

@Data
public class Article {
  ArticleSource source;
  String author,title,description,url,urlToImage,publishedAt,content;
}
