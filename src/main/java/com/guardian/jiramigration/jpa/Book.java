package com.guardian.jiramigration.jpa;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@ToString(exclude = "author") // prevent circular reference
@Entity
public class Book {

  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  String name;
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "author_id")
  Author author;
}
