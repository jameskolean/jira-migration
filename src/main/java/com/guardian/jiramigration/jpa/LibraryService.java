package com.guardian.jiramigration.jpa;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class LibraryService {
  @Autowired
  AuthorRepository authorRepository;
  @Autowired
  BookRepository bookRepository;
  @Autowired
  ModelMapper modelMapper;
  public List<AuthorDto> getAllAuthors() {
    return StreamSupport.stream(authorRepository.findAll().spliterator(), false).map(author -> convertToDto(author)).collect(Collectors.toList());
  }
  public List<BookDto> getAllBooks() {
    return StreamSupport.stream(bookRepository.findAll().spliterator(), false).map(book -> convertToDto(book)).collect(Collectors.toList());
  }
  private BookDto convertToDto(Book book) {
    BookDto bookDto = modelMapper.map(book, BookDto.class);
    return bookDto;
  }
  private AuthorDto convertToDto(Author author) {
    AuthorDto authorDto = modelMapper.map(author, AuthorDto.class);
    return authorDto;
  }

}
