package com.guardian.jiramigration.jpa;

import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Data
@Entity
public class Author {
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  String name;
  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="author")
  List<Book> books;
}