package com.guardian.jiramigration;

import lombok.Data;

@Data
public class ArticleResult {
  Article[] articles;
  String status;
  Integer totalResults;
}
