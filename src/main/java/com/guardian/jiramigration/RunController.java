package com.guardian.jiramigration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/run")
class RunController {

  @Autowired
  private RestTemplate restTemplate;

  @RequestMapping
  public ArticleResult run() {
    String url = "https://newsapi.org/v2/top-headlines?sources=techcrunch&sortBy=top&apiKey=44ddad9741b745168f18d2a558262c68";
    ResponseEntity<ArticleResult> forEntity = restTemplate.getForEntity(url, ArticleResult.class);
    return forEntity.getBody();
  }

}