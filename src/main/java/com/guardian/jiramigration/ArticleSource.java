package com.guardian.jiramigration;

import lombok.Data;

@Data
public class ArticleSource {
  String id, name;
}
